class DimmersController < DevicesController
  def update
    @device = Device.find params[:id]
    device_params = params[:dimmer]

    next_switch = device_params[:switch] == '1'
    next_level = device_params[:level].to_i

    @device.toggle if next_switch != @device.switch
    @device.save_level next_level if next_switch && @device.level != next_level

    super
  end
end
