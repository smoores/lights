class OutletsController < DevicesController
  def update
    @device = Device.find params[:id]
    device_params = params[:outlet]

    next_switch = device_params[:switch] == '1'
    @device.toggle if next_switch != @device.switch

    super
  end
end
