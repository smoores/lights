class DevicesController < ApplicationController
  def index
    @devices = Device.all.map { |d| Device.find d.id }.sort
  end

  def edit
    @device = Device.find params[:id]
  end

  def update
    sleep 3
    redirect_to action: 'edit', id: params[:id]
  end

  def toggle
    device = Device.find params[:id]
    device.toggle

    sleep 1.0 / 10.0
    redirect_to root_path
  end
end
