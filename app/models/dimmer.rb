class Dimmer < Device
  self.element_name = 'device'

  def category
    'dimmer'
  end

  def switch
    switch_attribute = attributes[:attributes].find { |attribute| attribute.name == 'switch' }
    switch_attribute.currentValue == 'on'
  end

  def level
    level_attribute = attributes[:attributes].find { |attribute| attribute.name == 'level' }
    level_attribute.currentValue
  end

  def toggle
    command = switch ? :off : :on
    execute_command(command)
  end

  def save_level(level)
    execute_command "setLevel/#{level}"
  end

  def on?
    switch
  end
end
