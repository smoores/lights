class Device < ActiveResource::Base
  self.site = "http://#{ENV['HUBITAT_HOST']}/apps/api/67/"
  self.include_format_in_path = false

  # Always include access token query param
  def self.collection_path(prefix_options = {}, query_options = {})
    puts 'getting collection path'
    super(
      prefix_options,
      query_options.merge(
        access_token: Rails.application.credentials.dig(:hubitat,
                                                        :access_token)
      ))
  end

  # Always include access token query param
  def self.element_path(id, prefix_options = {}, query_options = {})
    super(
      id,
      prefix_options,
      query_options.merge(
        access_token: Rails.application.credentials.dig(:hubitat,
                                                        :access_token)
      ))
  end

  def self.find(*args)
    return super(*args) if args[0] == :all

    device = super(*args)

    return Thermostat.new device.attributes if device.thermostat?
    return Outlet.new device.attributes if device.outlet?

    Dimmer.new device.attributes
  end

  def persisted?
    true
  end

  def <=>(other)
    category <=> other.category
  end

  def execute_command(command)
    get(command, access_token: Rails.application.credentials.dig(:hubitat, :access_token))
  end

  def outlet?
    type.include?('Outlet')
  end

  def dimmer?
    type.include?('Dimmer')
  end

  def thermostat?
    type.include?('Thermostat')
  end
end
