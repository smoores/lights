class Thermostat < Device
  self.element_name = 'device'

  def category
    'thermostat'
  end

  def thermostat_mode
    thermostat_mode_attribute = attributes[:attributes].find { |attribute| attribute.name == 'thermostatMode' }
    thermostat_mode_attribute.currentValue
  end

  def toggle
    next_mode = thermostat_mode == 'auto' ? :off : :auto
    execute_command("setThermostatMode/#{next_mode}")
  end

  def on?
    thermostat_mode != 'off'
  end

  def update; end
end
