class Outlet < Device
  self.element_name = 'device'

  def category
    'outlet'
  end

  def switch
    switch_attribute = attributes[:attributes].find { |attribute| attribute.name == 'switch' }
    switch_attribute.currentValue == 'on'
  end

  def toggle
    command = switch ? :off : :on
    execute_command(command)
  end

  def on?
    switch
  end
end
