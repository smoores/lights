Rails.application.routes.draw do
  root 'devices#index'
  resources :devices, only: :index
  resources :thermostats, :dimmers, :outlets do
    member do
      put 'toggle'
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
